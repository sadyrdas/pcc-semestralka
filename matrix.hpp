#include <vector>
#include <fstream>
#include <sstream>
//Class matrix with basic functionality for implementation to calculate determinant
class matrix {
private:
    bool sign=false;
    std::vector<std::vector<long double> > mat;
    unsigned rows;
    unsigned columns;

    void read(std::istream& is);
    void print(std::ostream& os) const;
public:
    matrix();
    matrix(unsigned _rows, unsigned _columns, const double& _initial);
    matrix(const matrix& rhs)=delete;

    // Access to size
    unsigned get_rows() const;
    unsigned get_cols() const;

    // Access to change elements
    long double& operator()(const unsigned& row, const unsigned& column);
    const long double& operator()(const unsigned& row, const unsigned& column) const;
    void changeElement(const unsigned& row, const unsigned& column, long double val);
    void swapRows(const unsigned& row1,const unsigned& row2);

    bool diagonalize();
    long double multiply_diagonal();

    bool valid() const;
    friend std::ostream& operator<<(std::ostream& os, const matrix& mtx);
    friend std::istream& operator>>(std::istream& is, matrix& mtx);
};



