//
// Created by KeHTo on 03.01.2023.
//

#include "matrix.hpp"

// Constructor
using namespace std;

matrix::matrix(unsigned _rows, unsigned _columns, const double &_initial) {
    mat.resize(_rows);
    for (unsigned i = 0; i < mat.size(); i++) {
        mat[i].resize(_columns, _initial);
    }
    rows = _rows;
    columns = _columns;
}


// Get rows of matrix
unsigned matrix::get_rows() const {
    return this->rows;
}

// Get columns of matrix
unsigned matrix::get_cols() const {
    return this->columns;
}

// Access to elements of matrix
long double &matrix::operator()(const unsigned &row, const unsigned &column) {
    return this->mat[row][column];
}

// Access to elements of matrix(const)
const long double &matrix::operator()(const unsigned &row, const unsigned &column) const {
    return this->mat[row][column];
}

// Change elements
void matrix::changeElement(const unsigned &row, const unsigned &column, long double val) {
    this->mat[row][column] = val;
}

// Swapping rows
void matrix::swapRows(const unsigned &row1, const unsigned &row2) {
    this->mat[row1].swap(this->mat[row2]);
    sign=!sign;
}

bool matrix::diagonalize() {
    matrix &matrix = *this;
    long double determinant = 1;
    for (int i = 0; i < matrix.get_rows(); ++i) {
        long double pivot = matrix(i, i);
        int pivotRow = i;

        //Find as small a pivot as possible
        for (int row = i + 1; row < matrix.get_rows(); ++row) {
            if ((abs(matrix(row, i)) < abs(pivot)) && abs(matrix(row, i)) != 0) {
                pivot = matrix(row, i);
                pivotRow = row;
                //Or at least a non-zero pivot if we have pivot = 0 at the beginning
            } else if (abs(pivot) == 0 && (abs(matrix(row, i)) > abs(pivot))) {
                pivot = matrix(row, i);
                pivotRow = row;
            }
        }

        //When pivot is zero, determinant is zero and matrix is not diaganalized
        if (pivot == 0.0) {
            return false;
        }
        //If we found a better pivot somewhere in the matrix, then we swap the rows and multiply the pivot by -1
        if (pivotRow != i) {
            matrix.swapRows(i, pivotRow);
            determinant *= -1.0;
        }
        //Then we multiply the determinant by the pivot, because it will not change anymore
        determinant *= pivot;

        //From the remaining lines I subtract the multiple of the line I just processed
        for (int row = i + 1; row < matrix.get_rows(); ++row) {
            long double multiple = (matrix(row, i) / pivot);
            for (int col = i; col < matrix.get_rows(); ++col) {
                matrix.changeElement(row, col, matrix(row, col) -= matrix(i, col) * multiple);
            }
        }
    }
    return true;
}

long double matrix::multiply_diagonal() {
    matrix &matrix = *this;
    long double res = 1;
    for (int i = 0; i < get_cols(); ++i)
        res *= matrix(i, i);
    if(sign)
        res*=-1;
    return res;
}

void matrix::read(istream &in) {
    while (!in.eof()){
        std::string line;
        std::getline(in,line,'\n');
        if (line.empty())
            break;
        std::istringstream lines(line);
        mat.emplace_back();
        while(!lines.eof()){
            long double elem;
            lines>>elem;
            mat.back().push_back(elem);
        }
    }
    rows=mat.size();
    columns=!mat.empty()? mat[0].size() : 0;
}

void matrix::print(ostream &os) const {
    const matrix &matrix = *this;

    for (int i = 0; i < matrix.get_rows(); i++) {
        for (int j = 0; j < matrix.get_cols(); j++)
            os << matrix(i, j)<<"  ";
        os<<std::endl;
    }
}

std::ostream &operator<<(ostream &os, const matrix &mtx) {
    mtx.print(os);
    return os;
}

std::istream &operator>>(istream &is, matrix &mtx) {
    mtx.read(is);
    return is;
}

matrix::matrix():rows(0),columns(0) {}

bool matrix::valid() const {
    for (int i = 0; i < rows; ++i)
        if(mat[i].size()!=columns)
            return false;
    return true;
}
