# pcc-semestralka



## Výpočet determinantu čtvercové matice
## Zadaní
Naimplementovat program pro výpočet determinantu čtvercové matice pomocí Gaussovy eliminační metody. Očekává se, že
tento program bude schopen pracovat s maticemi 1000x1000 v rozumném čase. Aplikace na vstupu dostane čtvercovou
matici, na výstupu vypíše hodnotu jejího determinantu.
## Popis
Kalkulačka pro výpočet determinantu čtvercové matice může přijmout na vstup matici z .txt souboru, nebo muzete napsat matice ručně.
## Implementace
Nejprve tento program převede matici na horní trojúhelníkovou matici pomocí Gaussovy eliminační metody, a poté vypočítá
determinant vynásobením hlavní diagonály.
## Spuštění
-f <file>  - dostane matici z .txt souboru s názvem <file>;
-h nebo -help nebo spusteni bez argumentu - vypise vam pomocnou informace
-c - povoluje zapsat matice rucne
## Ovladaní
Ovladaní pres Unix CLI
## Měření
Matice 1000x1000 determinant vypocte za 11 sekund. 100x100 za 1 ms.
