#include <iostream>
#include "matrix.hpp"
#include <cstring>

using namespace std;

void print_help(){
    cout<<"Hello, I am help"<<endl;
    cout<<"Usage:"<<endl;
    cout<<"-f file"<<endl;
    cout<<"-c"<<endl;
}
int main(int argc, char *argv[]){
    if (argc == 1 || strcmp(argv[1], "-h") == 0 || strcmp(argv[1], "-help") == 0) {
        print_help();
        return 0;
    }
    matrix matrix;
    if(argc==2 && strcmp(argv[1],"-c")==0){
        cout<<"Type your matrix manually"<<endl;
        cin>>matrix;
    }
    if(argc==3 && strcmp(argv[1],"-f")==0){
        ifstream file(argv[2]);
        file>>matrix;
    }
    if(!matrix.valid() || matrix.get_rows()!=matrix.get_cols()){
        cout<<"matrix is invalid"<<endl;
        return 1;
    }
    cout<<"Diagonalizing matrix"<<endl;
    long double det=matrix.diagonalize() ? matrix.multiply_diagonal() : 0;

    cout<<"Determinant: "<<det<<endl;
    return 0;
}
